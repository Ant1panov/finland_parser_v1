import random
from selenium import webdriver
from time import sleep
import openpyxl
from selenium.webdriver import Keys
from openpyxl import Workbook
from random import randint, choice
from selenium.webdriver.common.proxy import Proxy, ProxyType
from seleniumwire import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By


# Прописываю прокси
ip_list = ["85.195.81.169:13079", "85.195.81.169:13078", "85.195.81.169:13077"]
proxy_ip_port = random.choice(ip_list)
if proxy_ip_port == "85.195.81.169:13079":
    proxy_username = "XVHEkB"
    proxy_password = "fRGTZx"
elif proxy_ip_port == "85.195.81.169:13078":
    proxy_username = "XVHEkB"
    proxy_password = "fRGTZx"
else:
    proxy_username = "XVHEkB"
    proxy_password = "fRGTZx"

seleniumwire_options = {
    'proxy': {
        'http': f'http://{proxy_username}:{proxy_password}@{proxy_ip_port}',
        'verify_ssl': False,
    },
}

driver = webdriver.Chrome(
    seleniumwire_options=seleniumwire_options
)
book = openpyxl.open("Net_Price_ITR2024__(7)[1] (1).xlsx", read_only=True)
sheet = book.active

# Захожу на сайт
driver.get("https://www.hankkija.fi/varaosat-ja-tarvikkeet/")
# x = input("enter smth")
# После того, как драйвер откроет сайт, то нужно самому согласится с куками. ам откроется окно. Будет зелёная кнопка. Её надо самому нажать и всё заработает
sleep(5)

# Создаю списки для хранения данных
names_of_products = []
prices = []
not_found_products = []
product_article = []


# Начало цикла. В нём я прохожусь по каждой строчке в таблице и вставляю её в поиск
for i in range(2, 18):
    # Нахожу поиск и ввожу туда данные
    search_input = driver.find_element("xpath", "//input[@class='frosmo-instant-search-input h-search__input']")
    search_input.click()
    sleep(2)
    search_input.send_keys(sheet["A" + str(i)].value)
    product_article.append(str(sheet["A" + str(i)].value))
    sleep(1)
    search_input.send_keys(Keys.ENTER)
    sleep(5)
    try:
        # Если данные есть, то я их забираю и кладу в списки, котороые я создал выше
        name_of_product = driver.find_element("xpath", "//div[@class='fris-hit-name']").text
        print(name_of_product)
        price_of_product = driver.find_element("xpath", "//div[@class='fris-hit-info-container']//strong").text.replace("/KPL", "").replace("/PARI", "")
        print(price_of_product)
        names_of_products.append(name_of_product)
        prices.append(price_of_product)
    except:
        # Если нет, то пишу, что ничего нет и вписываю в списки, что ничего нет. Дальше это пойдёт в таблицу
        print("No product found")
        names_of_products.append("No product found")
        prices.append("No product found")

# Начинаю работу с записью в новую таблицу
workbook = Workbook()
sheet = workbook.active
sheet.cell(row=1, column=1, value='Product')
sheet.cell(row=1, column=2, value='Price')


for index, (product, price) in enumerate(zip(names_of_products, prices), start=2):
    sheet.cell(row=index, column=1, value=product)
    sheet.cell(row=index, column=2, value=price)
    # sheet.cell(row=index, column=3, value=product_article)


workbook.save('example.xlsx')
